var http = require('http');

var i = 2

var getData = function(){
	
	var stringarr = "";
	var url = process.argv[i];
	
	http.get(url, function(res) {
		
		//for checking if the data is correct or not
		if(res.statusCode == 200)
		{
			res.setEncoding("utf8");
			
			res.on("data", function(data) {	
			
				stringarr = stringarr + data;
			
			});//end of receiving data

			res.on("end",function(data){
					
					console.log(stringarr);
					
					i++;
					
					if(i<5)
					{
						getData();
					}//end of if
			});//end of end
		}//end of if
	}).on('error', function(e) {

	  console.log("Got error: " + e.message);

	});//end of error
}//end getData function

getData();
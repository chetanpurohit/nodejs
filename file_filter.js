var fs = require('fs');
var path = require('path');
var directory = process.argv[2];
var ext = process.argv[3];
 
fs.readdir(directory, function (err, files) {
    [].forEach.call(files, function (el) {
        if (path.extname(el).slice(1) === ext) {
            console.log(el);
        }
    });
});
var http = require('http');
var url = require('url');
var port = process.argv[2];

var getTime = function(urlPath){

	if(urlPath.pathname == '/api/parsetime')
	{
        var dt = new Date(urlPath.query.iso);

		return {
			"hour" : dt.getHours(),
			"minute" : dt.getMinutes(),
			"second" : dt.getSeconds()
		};//end of return
	}//end of if
	else if(urlPath.pathname == '/api/unixtime')
	{
		return {
			"unixtime" : new Date(urlPath.query.iso).getTime()
		};
	}
}//end of getTime function

//for creating server
var server = http.createServer(function(request,response)
{

	var urlPath = url.parse(request.url, true),
    resourse = getTime(urlPath);

    response.end(JSON.stringify(resourse));

    if (resourse) {
        response.writeHead(200, {
            'Content-Type': 'application/json'
        });
        response.end(JSON.stringify(resourse));
    } else {
        response.writeHead(404);
        response.end();
    }
});

server.listen(port);
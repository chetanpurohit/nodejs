var http = require('http');
var fs = require('fs');
var port = process.argv[2];
var path = process.argv[3];

var file = fs.createReadStream(path);
var server = http.createServer(function(request, response) {
    file.pipe(response);
});
server.listen(port);
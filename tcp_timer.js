var net = require('net');
var port = process.argv[2];

var server = net.createServer(function(socket) {
  	var dt = new Date();
  	var year = dt.getFullYear();
  	var month = dt.getMonth() + 1;
  	var day = dt.getDate();
  	var hour = dt.getHours();
  	var min = dt.getMinutes();
  	if(month < 10)
  		month = "0" + month;

  	if(day < 10)
  		day = "0" + day;

  	if(hour < 10)
  		hour = "0" + hour;

  	if(min < 10)
  		min = "0" + min;

  	var timer = year + "-" + month + "-" + day + " " + hour + ":" + min;
  	socket.end(timer);
});
server.listen(port);
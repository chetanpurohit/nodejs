var fs = require('fs');
var path = require('path');

//global function to filter directory
exports.filterDirectory = function(directory,ext)
{
	fs.readdir(directory, function (err, files) {
	    [].forEach.call(files, function (el) {
	        if (path.extname(el).slice(1) === ext) {
	            console.log(el);
	        }//end of if
	    });//end of for
	});//end of fs.readdir
}//end of function
